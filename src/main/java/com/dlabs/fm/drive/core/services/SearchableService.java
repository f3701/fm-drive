package com.dlabs.fm.drive.core.services;

import com.dlabs.fm.drive.core.repositories.BaseRepository;
import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.core.models.BaseModel;

import java.util.List;

public interface SearchableService<
        R extends BaseRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        >
        extends BaseService<R, E, ID>{

    int MAX_SIZE_RESULTS = 256;

    default ResponseDto findAll() {

        if (this.getRepository().findAll().size() > MAX_SIZE_RESULTS) {
            throw new BusinessException("The amount of results exceeds the maximum supported, use paging instead.");
        }

        List<E> results = this.getRepository().findAll();

        return ResponseDto.builder()
                .data(results)
                .build();
    }

    default ResponseDto findById(ID id) {

        E result = this.getRepository().findById(id)
                .orElseThrow(() -> new BusinessException(String.format("Id %s not exist.", id)));

        return ResponseDto.builder()
                .data(result)
                .build();

    }

    default List<E> findAllAsList() {

        if (this.getRepository().findAll().size() > MAX_SIZE_RESULTS) {
            throw new BusinessException("The amount of results exceeds the maximum supported, use paging instead.");
        }

        return this.getRepository().findAll();

    }

}
