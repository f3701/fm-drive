package com.dlabs.fm.drive.core;

// Exception not handled by the API.
public class BusinessException extends RuntimeException{

    public BusinessException(String message) {
        super(message);
    }

}
