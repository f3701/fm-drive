package com.dlabs.fm.drive.core.dtos;

import com.dlabs.fm.drive.core.Beans;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ResponseDto {

    private Object data;

    // Custom Setter for Builder
    public static class ResponseDtoBuilder {

        public ResponseDtoBuilder() {}

        public ResponseDtoBuilder data(Object data, Class dto) {
            this.data = Beans.modelMapper.map(data, dto);
            return this;
        }

        public ResponseDtoBuilder data(Object data) {
            this.data = data;
            return this;
        }
    }

}
