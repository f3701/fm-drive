package com.dlabs.fm.drive.core.services;

import com.dlabs.fm.drive.core.repositories.BaseRepository;
import com.dlabs.fm.drive.core.models.BaseModel;

public interface BaseService<
        R extends BaseRepository<E, I>,
        E extends BaseModel<I>,
        I
        > {

    R getRepository();

}
