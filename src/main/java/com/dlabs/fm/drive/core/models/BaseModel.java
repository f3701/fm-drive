package com.dlabs.fm.drive.core.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "deleted_at IS NULL")
public abstract class BaseModel<ID> {

    protected abstract void setId(ID id);
    protected abstract ID getId();

    @Column(name = "created_at")
    protected Date createdAt;

    @Column(name = "created_by")
    protected String createdBy;

    @Column(name = "deleted_at")
    protected Date deletedAt;

    @Column(name = "deleted_by")
    protected String deletedBy;

    @Column(name = "updated_last_at")
    protected Date updatedLastAt;

    @Column(name = "updated_last_by")
    protected String updatedLastBy;

    // Updating Audit fields
    @PrePersist
    public void prePersist() {
        this.setCreatedAt(new Date());
        this.setCreatedBy("SYSTEM");
    }
    // Updating Audit fields
    @PreUpdate
    public void preUpdate() {
        this.setUpdatedLastAt(new Date());
        this.setUpdatedLastBy("SYSTEM");
    }
}
