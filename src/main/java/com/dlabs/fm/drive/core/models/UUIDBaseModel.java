package com.dlabs.fm.drive.core.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class UUIDBaseModel extends BaseModel<UUID> {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    protected UUID id;

}
