package com.dlabs.fm.drive.core.services;

import com.dlabs.fm.drive.core.repositories.DeletableRepository;
import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.core.models.BaseModel;

import java.util.Date;

public interface DeletableService<
        R extends DeletableRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        >
        extends BaseService<R, E, ID> {

    default void deleteById(ID id) {

        if (!this.getRepository().existsById(id)) {
            throw new BusinessException(String.format("Id %s not exist", id));
        }

        this.getRepository().deleteById(id, new Date(), "System");

    }

}
