package com.dlabs.fm.drive.modules.news.services;

import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.modules.files.services.ImageService;
import com.dlabs.fm.drive.modules.news.repositories.NewsImageRepository;
import com.dlabs.fm.drive.modules.news.repositories.NewsStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Service
public class NewsValidator {

    @Autowired
    private NewsImageRepository newsImageRepository;

    @Autowired
    private NewsStatusRepository newsStatusRepository;

    @Autowired
    private ImageService imageService;

    /**
     * Validate format file
     * Validate size file
     *
     * @param file to validate
     */
    public void validate(MultipartFile file) {

        this.validateDuplicated(file);

        this.imageService.validateFormat(file);

        this.validateSize(file);

    }

    private void validateDuplicated(MultipartFile file) {

        String fileName = file.getOriginalFilename();

        if (this.newsImageRepository.existsByName(fileName)) {
            throw new BusinessException("File already exist: " + fileName);
        }

    }

    public void validateSize(MultipartFile file) {
        // Done by spring property: max-file-size
    }

    public void validateNewsImageId(UUID id) {

        if (!this.newsImageRepository.existsById(id)) {
            throw new BusinessException("News Image ID not exists: " + id);
        }

    }

    public void validateNewsStatusByName(String name) {

        if (!this.newsStatusRepository.existsByName(name)) {
            throw new BusinessException("News Status NAME not exists: " + name);
        }

    }
}
