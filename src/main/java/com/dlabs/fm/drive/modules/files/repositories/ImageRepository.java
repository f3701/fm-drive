package com.dlabs.fm.drive.modules.files.repositories;

import com.dlabs.fm.drive.modules.files.models.ImageModel;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ImageRepository<T extends ImageModel> extends FileRepository<T> {

}

