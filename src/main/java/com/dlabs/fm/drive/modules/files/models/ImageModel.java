package com.dlabs.fm.drive.modules.files.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ImageModel extends FileModel {

    @Column(name = "width")
    protected BigDecimal width;

    @Column(name = "height")
    protected BigDecimal height;

}
