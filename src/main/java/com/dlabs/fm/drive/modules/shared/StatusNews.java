package com.dlabs.fm.drive.modules.shared;

public enum StatusNews {
    ;
    public static final String ARCHIVED = "ARCHIVED";

    public static final String ACTIVE = "ACTIVE";
}
