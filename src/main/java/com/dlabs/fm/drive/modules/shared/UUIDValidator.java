package com.dlabs.fm.drive.modules.shared;

import com.dlabs.fm.drive.core.BusinessException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDValidator {


    public void validateFormat(String uuidStr) {
        try {
             UUID.fromString(uuidStr);
        } catch (IllegalArgumentException e) {
            throw new BusinessException("Invalid UUID format. " + uuidStr);
        }
    }
}

