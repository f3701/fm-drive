package com.dlabs.fm.drive.modules.products.repositories;

import com.dlabs.fm.drive.modules.files.repositories.ImageRepository;
import com.dlabs.fm.drive.modules.products.models.ProductImageModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface ProductImageRepository extends ImageRepository<ProductImageModel>, JpaSpecificationExecutor<ProductImageModel> {


    @Query(" SELECT e " +
            "FROM #{#entityName} e " +
            "WHERE e.productId = :productId " +
            "AND e.deletedAt IS NULl ")
    List<ProductImageModel> findAllByProductId(UUID productId);

    @Query(" SELECT COUNT(e) " +
            "FROM #{#entityName} e " +
            "WHERE e.productId = :productId " +
            "AND e.deletedAt IS NULl ")
    Integer countByProductId(UUID productId);

    @Query(" SELECT e " +
            "FROM #{#entityName} e " +
            "WHERE e.productId = :productId " +
            "AND e.order = :order " +
            "AND e.deletedAt IS NULl ")
    ProductImageModel findByProductIdAndOrder(UUID productId, Integer order);

    @Query(" SELECT e " +
            "FROM #{#entityName} e " +
            "WHERE e.productId = :productId " +
            "AND e.order > :order " +
            "AND e.deletedAt IS NULl ")
    List<ProductImageModel> findAllByProductIdAndOrderGraterThan(UUID productId, Integer order);

    /**
     * Delete records (soft-delete)
     *
     * @param productId records
     */
    @Query("UPDATE #{#entityName} e SET e.deletedAt = :date, e.deletedBy = :user WHERE e.productId = :productId ")
    @Modifying
    void deleteByProductId(@Param("productId") UUID productId, @Param("date") Date date, @Param("user") String user);

}

