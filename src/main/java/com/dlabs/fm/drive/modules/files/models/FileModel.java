package com.dlabs.fm.drive.modules.files.models;

import com.dlabs.fm.drive.core.models.UUIDBaseModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class FileModel extends UUIDBaseModel {

    @Column(name = "name")
    protected String name;

    @Column(name = "mimetype")
    protected String mimetype;

    @Column(name = "path")
    protected String path;

    @Column(name = "size")
    protected BigDecimal size;

}
