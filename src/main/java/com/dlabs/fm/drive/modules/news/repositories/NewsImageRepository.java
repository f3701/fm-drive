package com.dlabs.fm.drive.modules.news.repositories;

import com.dlabs.fm.drive.modules.news.models.NewsImageModel;
import com.dlabs.fm.drive.modules.files.repositories.ImageRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsImageRepository extends ImageRepository<NewsImageModel>, JpaSpecificationExecutor<NewsImageModel>
{

}

