package com.dlabs.fm.drive.modules.news.models;

import com.dlabs.fm.drive.core.models.UUIDBaseModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "news_status")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NewsStatusModel extends UUIDBaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

}
