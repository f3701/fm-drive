package com.dlabs.fm.drive.modules.news.dtos;

import com.dlabs.fm.drive.core.dtos.OutUUIDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsStatusOutDto extends OutUUIDto {

    private String name;

}
