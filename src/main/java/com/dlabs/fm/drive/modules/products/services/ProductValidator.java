package com.dlabs.fm.drive.modules.products.services;

import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.modules.products.models.ProductImageModel;
import com.dlabs.fm.drive.modules.products.repositories.ProductImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

@Service
public class ProductValidator {

    @Autowired
    private ProductImageRepository productImageRepository;

    /**
     * Validate if file already exist.
     * If exist throws exception otherwise void return.
     * @param fileName target
     */
    public void validateDuplicatedByFilename(UUID productId, String fileName) {

        List<ProductImageModel> productImages = this.productImageRepository.findAllByProductId(productId);
        productImages.forEach(imageModel -> {
            if (imageModel.getName().equals(fileName)) {
                throw new BusinessException("File already exist: " + fileName);
            }
        });
    }

    /**
     * Validate if file product model exists.
     * Throws BusinessException if not exist.
     * @param id of model
     */
    public void assertProductFileExistById(UUID id) {

        if (!this.productImageRepository.existsById(id)) {
            throw new BusinessException("File not exist with this id: " + id);
        }

    }

    public void validateOrderImage (Integer order) {
        if (order < 0) {
            throw new BusinessException("Order must be a positive number: " + order);
        }
    }

    public void validateExistence(UUID id) {
        if (!this.productImageRepository.existsById(id)) {
            throw new BusinessException("Invalid product image id: " + id);
        }
    }
}
