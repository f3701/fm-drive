package com.dlabs.fm.drive.modules.news.dtos;

import com.dlabs.fm.drive.core.dtos.InDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsQueryParamsDto extends InDto {

    private String status;

}
