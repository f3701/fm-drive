package com.dlabs.fm.drive.modules.files.exceptions;

import com.dlabs.fm.drive.core.BusinessException;

public class InvalidFormatFileException extends BusinessException {

    public InvalidFormatFileException(String message) {
        super(message);
    }
}
