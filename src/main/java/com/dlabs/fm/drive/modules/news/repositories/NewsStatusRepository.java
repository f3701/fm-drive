package com.dlabs.fm.drive.modules.news.repositories;

import com.dlabs.fm.drive.core.repositories.BaseRepository;
import com.dlabs.fm.drive.modules.news.models.NewsStatusModel;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsStatusRepository extends BaseRepository<NewsStatusModel, UUID> {

    NewsStatusModel findByName(String archived);

    boolean existsByName(String name);

    NewsStatusModel getOneByName(String status);
}

