package com.dlabs.fm.drive.modules.products.dtos;

import com.dlabs.fm.drive.core.dtos.OutUUIDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductImageOutDto extends OutUUIDto {

    private String productId;
    private Integer order;

}
