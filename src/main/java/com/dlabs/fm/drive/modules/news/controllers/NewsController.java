package com.dlabs.fm.drive.modules.news.controllers;

import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.modules.news.dtos.NewsImageInDto;
import com.dlabs.fm.drive.modules.news.dtos.NewsQueryParamsDto;
import com.dlabs.fm.drive.modules.news.services.NewsService;
import com.dlabs.fm.drive.modules.shared.URLs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping(URLs.BASE_URL)
public class NewsController {

    @Autowired
    private NewsService service;

    public static final String UPLOAD_IMAGE_ENDPOINT = "/news/images";
    public static final String DOWNLOAD_IMAGE_ENDPOINT = "/news/images/{id}";
    public static final String DELETE_IMAGE_ENDPOINT = "/news/images/{id}";
    public static final String UPDATE_IMAGE_DETAILS_ENDPOINT = "/news/images/{id}";
    public static final String GET_IMAGES_DETAILS_ENDPOINT = "/news/images";

    @PostMapping(value = UPLOAD_IMAGE_ENDPOINT,
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseDto create(@RequestPart(value = "file", required = true) MultipartFile file) throws IOException {

        return this.service.create(file);

    }

    @PutMapping(UPDATE_IMAGE_DETAILS_ENDPOINT)
    public ResponseDto update(@RequestBody NewsImageInDto dto, @PathVariable UUID id) {

        return this.service.update(dto, id);

    }

    @DeleteMapping(DELETE_IMAGE_ENDPOINT)
    public ResponseDto delete(@PathVariable UUID id) {

        return this.service.delete(id);

    }

    @GetMapping(GET_IMAGES_DETAILS_ENDPOINT)
    public ResponseDto search(NewsQueryParamsDto dto) {

        return this.service.search(dto);

    }

    @GetMapping(DOWNLOAD_IMAGE_ENDPOINT)
    public ResponseEntity<Resource> getFile(@PathVariable UUID id) {

        return this.service.getFile(id);

    }

}
