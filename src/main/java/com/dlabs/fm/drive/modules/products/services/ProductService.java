package com.dlabs.fm.drive.modules.products.services;

import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.core.services.SearchableService;
import com.dlabs.fm.drive.modules.files.services.FileManager;
import com.dlabs.fm.drive.modules.files.services.ImageService;
import com.dlabs.fm.drive.modules.products.dtos.ProductImageOutDto;
import com.dlabs.fm.drive.modules.products.models.ProductImageModel;
import com.dlabs.fm.drive.modules.products.repositories.ProductImageRepository;
import com.dlabs.fm.drive.modules.shared.UUIDValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class ProductService implements
        SearchableService<
                ProductImageRepository,
                ProductImageModel,
                UUID
                > {

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private FileManager fileManager;
    public static String PATH_UPLOAD_STR = "./products";

    @Autowired
    private UUIDValidator uuidValidator;

    @Autowired
    private ProductValidator productValidator;

    private Path PATH_UPLOAD;

    @PostConstruct
    public void createNewsDirectories() {
        PATH_UPLOAD = fileManager.createDirectory(PATH_UPLOAD_STR);
    }

    @Override
    public ProductImageRepository getRepository() {

        return this.productImageRepository;

    }

    public ResponseDto uploadProductImage(MultipartFile file, String productId) throws IOException {

        // Validations
        this.imageService.validateFormat(file);
        this.uuidValidator.validateFormat(productId);
        this.productValidator.validateDuplicatedByFilename(UUID.fromString(productId), file.getOriginalFilename());

        // Create product directory if not exist.
        Path productDirectory = this.fileManager.createDirectory(PATH_UPLOAD_STR + "/" + productId);
        this.fileManager.createFile(productDirectory, file.getInputStream(), file.getOriginalFilename());

        BufferedImage imageBuffered = imageService.imageBufferedFrom(file);

        // Save product model
        ProductImageModel model = ProductImageModel.builder()
                .size(BigDecimal.valueOf(file.getSize()))
                .name(file.getOriginalFilename())
                .mimetype(file.getContentType())
                .height(BigDecimal.valueOf(imageBuffered.getHeight()))
                .width(BigDecimal.valueOf(imageBuffered.getWidth()))
                .productId(UUID.fromString(productId))
                .order(this.productImageRepository.countByProductId(UUID.fromString(productId)))
                .build();

        model = productImageRepository.save(model);

        // Dto
        ProductImageOutDto dto = modelMapper.map(model, ProductImageOutDto.class);

        return ResponseDto.builder().data(dto).build();

    }

    public ResponseEntity<Resource> downloadProductImage(String productId, Integer order) {

        this.uuidValidator.validateFormat(productId);
        this.productValidator.validateOrderImage(order);

        UUID id = UUID.fromString(productId);

        ProductImageModel model = this.productImageRepository.findByProductIdAndOrder(id, order);
        if (model == null) {
            throw new BusinessException("Invalid product id or order.");
        }

        Path productPath = this.fileManager.getPathDirectory(PATH_UPLOAD_STR + "/" + productId);
        Resource resource = this.fileManager.getFileAsResource(model.getName(), productPath);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(model.getMimetype()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "atachment; filename=\"" + model.getName() + "\"")
                .body(resource);
    }

    public ResponseEntity<Resource> downloadById(String id) {

        this.uuidValidator.validateFormat(id);
        this.productValidator.validateExistence(UUID.fromString(id));

        ProductImageModel model = this.productImageRepository.getOne(UUID.fromString(id));
        Path productPath = this.fileManager.getPathDirectory(PATH_UPLOAD_STR + "/" + model.getProductId());
        Resource resource = this.fileManager.getFileAsResource(model.getName(), productPath);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(model.getMimetype()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "atachment; filename=\"" + model.getName() + "\"")
                .body(resource);
    }

    public ResponseDto findImagesByProductId(String productId) {

        this.uuidValidator.validateFormat(productId);

        List<ProductImageModel> productImages = this.productImageRepository.findAllByProductId(UUID.fromString(productId));

        // Dto
        ProductImageOutDto[] dto = modelMapper.map(productImages, ProductImageOutDto[].class);

        return ResponseDto.builder().data(dto).build();
    }

    /**
     * Delete product image from data model and file system.
     *
     * @param modelId product image
     * @return dto deleted
     */
    public ResponseDto deleteProductImageById(String modelId) {

        this.uuidValidator.validateFormat(modelId);
        this.productValidator.validateExistence(UUID.fromString(modelId));

        // Model to delete
        ProductImageModel model = this.productImageRepository.getOne(UUID.fromString(modelId));

        // Delete from data model
        this.productImageRepository.deleteById(UUID.fromString(modelId), new Date(), "System");

        // Delete from file system
        this.deleteProductImageFile(model.getProductId().toString(), model.getName());

        // Reorganizing orders
        List<ProductImageModel> models = this.productImageRepository.findAllByProductIdAndOrderGraterThan(model.getProductId(), model.getOrder());
        List<ProductImageModel> modelsReordered = models.stream()
                .peek(m -> m.setOrder(m.getOrder() - 1))
                .collect(Collectors.toList());
        this.productImageRepository.saveAll(modelsReordered);

        return ResponseDto.builder().data(Map.of("message", "Deleted successfully")).build();

    }

    public ResponseDto deleteImagesForProduct(String productId) {

        this.uuidValidator.validateFormat(productId);

        // Models to delete
        List<ProductImageModel> models = this.productImageRepository.findAllByProductId(UUID.fromString(productId));

        // Delete from data model
        this.productImageRepository.deleteByProductId(UUID.fromString(productId), new Date(), "System");

        // Delete product images from filesystem
        models.forEach(
                productImage -> this.deleteProductImageFile(productId, productImage.getName())
        );

        // Delete productId directory
        this.fileManager.deleteDirectory(this.PATH_UPLOAD.resolve(productId));

        return ResponseDto.builder().data(Map.of("message", "Deleted successfully")).build();

    }

    private void deleteProductImageFile(String productId, String name) {
        try {
            this.fileManager.deleteFile(name, this.PATH_UPLOAD.resolve(productId));
        } catch(Exception e) {
            throw new BusinessException("Could not delete one file associated to product " + productId);
        }
    }

}

