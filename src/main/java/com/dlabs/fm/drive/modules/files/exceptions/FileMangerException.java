package com.dlabs.fm.drive.modules.files.exceptions;

import com.dlabs.fm.drive.core.BusinessException;

public class FileMangerException extends BusinessException {
    public FileMangerException(String message) {
        super(message);
    }
}
