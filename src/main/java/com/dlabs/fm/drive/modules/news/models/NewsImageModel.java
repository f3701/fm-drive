package com.dlabs.fm.drive.modules.news.models;

import com.dlabs.fm.drive.modules.files.models.ImageModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "news_images")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class NewsImageModel extends ImageModel {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "news_status_id")
    protected NewsStatusModel newsStatus;

}
