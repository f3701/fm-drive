package com.dlabs.fm.drive.modules.files.repositories;

import com.dlabs.fm.drive.core.repositories.DeletableRepository;
import com.dlabs.fm.drive.modules.files.models.FileModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.UUID;

@NoRepositoryBean
public interface FileRepository<T extends FileModel> extends DeletableRepository<T, UUID> {

    @Query(" SELECT CASE " +
            "WHEN count(e)> 0 " +
            "THEN true ELSE false " +
            "END " +
            "FROM #{#entityName} e " +
            "WHERE e.name = :name " +
            "AND e.deletedAt IS NULl ")
    boolean existsByName(String name);

}

