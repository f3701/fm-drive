package com.dlabs.fm.drive.modules.news.dtos;

import com.dlabs.fm.drive.core.dtos.InDto;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class NewsImageInDto extends InDto {

    private UUID newsStatusId;

    private String status;

}
