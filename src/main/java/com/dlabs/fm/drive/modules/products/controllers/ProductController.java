package com.dlabs.fm.drive.modules.products.controllers;

import com.dlabs.fm.drive.core.controllers.SearchableController;
import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.modules.products.models.ProductImageModel;
import com.dlabs.fm.drive.modules.products.repositories.ProductImageRepository;
import com.dlabs.fm.drive.modules.products.services.ProductService;
import com.dlabs.fm.drive.modules.shared.URLs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@Slf4j
public class ProductController implements
        SearchableController<
                ProductService,
                ProductImageRepository,
                ProductImageModel,
                UUID
                > {

    @Autowired
    private ProductService service;

    public static final String UPLOAD_IMAGE_ENDPOINT = URLs.BASE_URL + "/products/{productId}/images";

    public static final String DOWNLOAD_IMAGE_ENDPOINT = URLs.BASE_URL + "/products/{productId}/images/{order}";
    public static final String DOWNLOAD_IMAGE_BY_ID_ENDPOINT = URLs.BASE_URL + "/products/images/{id}";

    public static final String FETCH_ALL_IMAGES_ENDPOINT = URLs.BASE_URL + "/products/images";
    public static final String FETCH_ALL_IMAGES_BY_PRODUCT_ID_ENDPOINT = URLs.BASE_URL + "/products/{productId}/images";

    public static final String DELETE_IMAGES_BY_PRODUCT_ID_ENDPOINT = URLs.BASE_URL + "/products/{productId}/images";
    public static final String DELETE_IMAGE_ENDPOINT = URLs.BASE_URL + "/products/images/{id}";

    @Override
    public ProductService getService() {
        return this.service;
    }

    @Override
    public ResponseDto findById(UUID id) {
        return ResponseDto.builder().build();
    }

    @Override
    @GetMapping(FETCH_ALL_IMAGES_ENDPOINT)
    public ResponseDto findAll() {
        return SearchableController.super.findAll();
    }

    @PostMapping(
            value = UPLOAD_IMAGE_ENDPOINT,
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}
    )
    public ResponseDto upload(
            @RequestPart(value = "file", required = true) MultipartFile file,
            @PathVariable String productId
    ) throws IOException {

        return this.service.uploadProductImage(file, productId);
    }

    @GetMapping(DOWNLOAD_IMAGE_ENDPOINT)
    public ResponseEntity<Resource> download(
            @PathVariable String productId,
            @PathVariable Integer order
    ) {

        return this.service.downloadProductImage(productId, order);

    }

    @GetMapping(DOWNLOAD_IMAGE_BY_ID_ENDPOINT)
    public ResponseEntity<Resource> downloadById(
            @PathVariable String id
    ) {

        return this.service.downloadById(id);

    }

    @GetMapping(FETCH_ALL_IMAGES_BY_PRODUCT_ID_ENDPOINT)
    public ResponseDto findImagesByProductId(@PathVariable String productId) {

        return this.service.findImagesByProductId(productId);

    }

    @DeleteMapping(DELETE_IMAGE_ENDPOINT)
    public ResponseDto deleteById(
            @PathVariable String id
    ) {

        return this.service.deleteProductImageById(id);

    }

    @DeleteMapping(DELETE_IMAGES_BY_PRODUCT_ID_ENDPOINT)
    public ResponseDto delete(
            @PathVariable String productId
    ) {
        return this.service.deleteImagesForProduct(productId);
    }

}
