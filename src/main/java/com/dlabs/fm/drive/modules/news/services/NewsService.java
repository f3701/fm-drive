package com.dlabs.fm.drive.modules.news.services;

import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.core.services.SearchableService;
import com.dlabs.fm.drive.modules.files.services.FileManager;
import com.dlabs.fm.drive.modules.files.services.ImageService;
import com.dlabs.fm.drive.modules.news.dtos.NewsImageInDto;
import com.dlabs.fm.drive.modules.news.dtos.NewsImageOutDto;
import com.dlabs.fm.drive.modules.news.dtos.NewsQueryParamsDto;
import com.dlabs.fm.drive.modules.news.models.NewsImageModel;
import com.dlabs.fm.drive.modules.news.models.NewsStatusModel;
import com.dlabs.fm.drive.modules.news.repositories.NewsImageRepository;
import com.dlabs.fm.drive.modules.news.repositories.NewsStatusRepository;
import com.dlabs.fm.drive.modules.news.specifications.NewsSpecification;
import com.dlabs.fm.drive.modules.shared.StatusNews;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional(rollbackFor = Exception.class)
public class NewsService implements
        SearchableService<
                NewsImageRepository,
                NewsImageModel,
                UUID
                > {

    @Autowired
    private NewsValidator newsValidator;

    @Autowired
    private NewsImageRepository newsImageRepository;

    @Autowired
    private NewsStatusRepository newsStatusRepository;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ModelMapper modelMapper;

    // Start file manager injection
    @Autowired
    private FileManager fileManager;

    public static String PATH_UPLOAD_NEWS = "./news";
    private Path PATH_NEWS;

    @PostConstruct
    public void createNewsDirectories() {
        PATH_NEWS = fileManager.createDirectory(PATH_UPLOAD_NEWS);
    }
    // End file manager

    /**
     * Creates a new image file of news.
     *
     * @param file to save
     * @return dto with id of new file
     */
    public ResponseDto create(MultipartFile file) throws IOException {

        this.newsValidator.validate(file);

        this.fileManager.createFile(PATH_NEWS, file.getInputStream(), file.getOriginalFilename());

        BufferedImage imageBuffered = imageService.imageBufferedFrom(file);

        NewsImageModel model = NewsImageModel.builder()
                .newsStatus(newsStatusRepository.findByName(StatusNews.ACTIVE))
                .size(BigDecimal.valueOf(file.getSize()))
                .name(file.getOriginalFilename())
                .mimetype(file.getContentType())
                .height(BigDecimal.valueOf(imageBuffered.getHeight()))
                .width(BigDecimal.valueOf(imageBuffered.getWidth()))
                .build();

        model = newsImageRepository.save(model);

        NewsImageOutDto dto = modelMapper.map(model, NewsImageOutDto.class);

        return ResponseDto.builder().data(dto).build();

    }

    /**
     * Permits to change the status of the news image.
     * ARCHIVED / ACTIVE
     *
     * @param dto     contains new data
     * @param idImage of news image
     * @return idImage and new status
     */
    public ResponseDto update(NewsImageInDto dto, UUID idImage) {

        this.newsValidator.validateNewsImageId(idImage);
        this.newsValidator.validateNewsStatusByName(dto.getStatus());

        NewsImageModel newsImage = this.newsImageRepository.getOne(idImage);
        NewsStatusModel newsStatus = this.newsStatusRepository.getOneByName(dto.getStatus());

        // Update status
        newsImage.setNewsStatus(newsStatus);
        NewsImageModel modelUpdated = this.newsImageRepository.save(newsImage);

        NewsImageOutDto dtoUpdated = modelMapper.map(modelUpdated, NewsImageOutDto.class);

        return ResponseDto.builder().data(dtoUpdated).build();

    }

    /**
     * Deletes a news image by id.
     * The file will be removed from the filesystem.
     *
     * @param idImage of news image to delete
     */
    public ResponseDto delete(UUID idImage) {

        this.newsValidator.validateNewsImageId(idImage);

        NewsImageModel model = this.newsImageRepository.getOne(idImage);

        this.newsImageRepository.softDeleteById(idImage);

        this.fileManager.deleteFile(model.getName(), this.PATH_NEWS);

        return ResponseDto.builder()
                .data(Map.of("message", "Deleted successfully."))
                .build();

    }

    public ResponseDto search(NewsQueryParamsDto dto) {

        List<NewsImageModel> results = this.newsImageRepository.findAll(buildSpecification(dto));

        return ResponseDto.builder()
                .data(results, NewsImageOutDto[].class)
                .build();
    }

    private NewsSpecification buildSpecification(NewsQueryParamsDto dto) {
        NewsSpecification spec = new NewsSpecification();

        if (this.newsStatusRepository.existsByName(dto.getStatus())) {
            spec.setNewsStatusId(this.newsStatusRepository.getOneByName(dto.getStatus()).getId());
        }

        return spec;
    }

    @Override
    public NewsImageRepository getRepository() {
        return this.newsImageRepository;
    }

    /**
     * Get the file of news image
     *
     * @param id of news image
     * @return file
     */
    public ResponseEntity<Resource> getFile(UUID id) {

        this.newsValidator.validateNewsImageId(id);

        NewsImageModel model = this.newsImageRepository.getOne(id);
        Resource resource = this.fileManager.getFileAsResource(model.getName(), this.PATH_NEWS);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(model.getMimetype()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "atachment; filename=\"" + model.getName() + "\"")
                .body(resource);
    }

}

