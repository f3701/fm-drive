package com.dlabs.fm.drive.modules.news.specifications;

import com.dlabs.fm.drive.modules.news.models.NewsImageModel;
import com.dlabs.fm.drive.modules.news.models.NewsStatusModel;
import lombok.Data;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class NewsSpecification implements Specification<NewsImageModel> {

    private UUID newsStatusId;

    @Override
    public Predicate toPredicate(Root<NewsImageModel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();

//        if (this.getText() != null) {
//            List<Predicate> predicateList = new ArrayList<>();
//
//            predicateList.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("code")), "%" + this.getText().toLowerCase() + "%"));
//            predicateList.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + this.getText().toLowerCase() + "%"));
//
//            Predicate[] array = new Predicate[predicateList.size()];
//            predicates.add(criteriaBuilder.or(predicateList.toArray(array)));
//        }

        if (this.getNewsStatusId() != null){
            Join<NewsImageModel, NewsStatusModel> join = root.join("newsStatus", JoinType.INNER);
            predicates.add(criteriaBuilder.equal(join.get("id"), this.getNewsStatusId()));
        }

        predicates.add(criteriaBuilder.isNull(root.get("deletedAt")));

        return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));

    }

}
