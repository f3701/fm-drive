package com.dlabs.fm.drive.modules.products.models;

import com.dlabs.fm.drive.modules.files.models.ImageModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "product_images")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ProductImageModel extends ImageModel {

    @Column(name="product_id", columnDefinition = "uuid")
    protected UUID productId;

    @Column(name="image_order")
    protected Integer order;

}
