package com.dlabs.fm.drive.modules.files.services;

import com.dlabs.fm.drive.modules.files.exceptions.FileMangerException;

import java.io.InputStream;
import java.nio.file.Path;

public interface IFileManager {

    Boolean existsDirectory(String directory) throws FileMangerException;

    Path createDirectory(String directory) throws FileMangerException;

    Path createDirectories(String directories) throws FileMangerException;

    Path getPathDirectory(String directory) throws FileMangerException;

    <T extends InputStream> Object createFile(Path directory, T data, String fullName) throws FileMangerException;

    void deleteDirectoryRecursively(Path directory) throws FileMangerException;

    void deleteFile(String filename, Path relativePath) throws FileMangerException;

    void deleteDirectory(Path relativePath) throws FileMangerException;

    Path pathBase();

}
