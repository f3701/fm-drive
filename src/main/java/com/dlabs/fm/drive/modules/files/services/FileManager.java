package com.dlabs.fm.drive.modules.files.services;

import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.modules.files.exceptions.FileMangerException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;

@Service
public class FileManager implements IFileManager {

    private Path pathBase;

    private final String UPLOAD_DIRECTORY = "./uploads/dev";

    public FileManager() {
        try {
            this.pathBase = Paths.get(UPLOAD_DIRECTORY).toAbsolutePath().normalize();
            Files.createDirectories(this.pathBase);
        } catch (Exception e) {
            throw new FileMangerException("Could not create base directory for the File Manager.");
        }
    }

    @Override
    public Boolean existsDirectory(String directory) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Path createDirectory(String directory) {
        return this.createDirectories(directory);
    }

    @Override
    public Path createDirectories(String directories) {
        try {
            Path path = this.pathBase.resolve(directories);
            Files.createDirectories(path);
            return path;
        } catch (Exception e) {
            throw new FileMangerException("Could not create directory or directories. " + directories);
        }
    }

    @Override
    public Path getPathDirectory(String directory) {
        return this.pathBase.resolve(directory);
    }

    @Override
    public Object createFile(Path directory, InputStream data, String fullName) {
        try {
            Path path = directory.resolve(fullName);
            Files.copy(data, path, StandardCopyOption.REPLACE_EXISTING);
            return path;
        } catch (Exception e) {
            throw new FileMangerException("Could not create the file. " + fullName);
        }
    }

    @Override
    public void deleteDirectoryRecursively(Path directory) {
        try {
            Files.walk(directory)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (Exception e) {
            throw new FileMangerException("Could not delete directory. " + directory);
        }
    }

    @Override
    public void deleteFile(String filename, Path relativePath) throws FileMangerException {
        try {
            Path filePath = this.pathBase.resolve(relativePath).resolve(filename).normalize();
            Files.delete(filePath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileMangerException("Could not delete file.");
        }
    }

    @Override
    public void deleteDirectory(Path relativePath) throws FileMangerException {
        try {
            Path filePath = this.pathBase.resolve(relativePath).normalize();
            Files.deleteIfExists(filePath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileMangerException("Could not delete the directory.");
        }
    }

    @Override
    public Path pathBase() {
        return this.pathBase;
    }

    public Resource getFileAsResource(String fileName, Path relativePath) {
        try {

            Path filePath = this.pathBase.resolve(relativePath).resolve(fileName).normalize();

            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new BusinessException("File not found " + fileName);
            }
        } catch (Exception ex) {
            throw new BusinessException("File not found " + fileName);
        }
    }

}
