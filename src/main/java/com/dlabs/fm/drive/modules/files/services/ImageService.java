package com.dlabs.fm.drive.modules.files.services;

import com.dlabs.fm.drive.core.BusinessException;
import com.dlabs.fm.drive.modules.files.exceptions.InvalidFormatFileException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

@Service
public class ImageService {

    /**
     * Return file as image.
     * Then we could use its properties like height and width.
     * Accepts: JPG, JPEG, PNG, WEBP.
     * @param file multipart
     * @return file as image buffered
     */
    public BufferedImage imageBufferedFrom(MultipartFile file) {
        // TODO: unit tests
        BufferedImage image = null;
        try {
            image = ImageIO.read(file.getInputStream());
            if (image == null) { //If image=null means that the upload is not an image format
                throw new BusinessException("Invalid file, could not process the image.");
            }
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return image;
    }

    /**
     * Validate format file, these are accepted:
     * - JPG
     * - JPEG
     * - PNG
     * - WEBP
     *
     * @param file to validate
     */
    public void validateFormat(MultipartFile file) {

        if (Objects.equals(file.getContentType(), "image/jpg")) return;
        if (Objects.equals(file.getContentType(), "image/jpeg")) return;
        if (Objects.equals(file.getContentType(), "image/png")) return;
        if (Objects.equals(file.getContentType(), "image/webp")) return;

        throw new InvalidFormatFileException(file.getContentType() + " is not supported for news files.");

    }

}
