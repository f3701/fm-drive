ALTER TABLE drive.news_status
    ADD CONSTRAINT PK_news_status PRIMARY KEY (id),
    ADD CONSTRAINT UQ_name_news_status UNIQUE (name)
 ;
