CREATE TABLE drive.product_images
(
    -- Base
    id              uuid                  DEFAULT uuid_generate_v4(),

    -- Auditory
    created_by      VARCHAR(256) NOT NULL DEFAULT 'SYSTEM',
    created_at      TIMESTAMP    NOT NULL DEFAULT NOW(),

    updated_last_by VARCHAR(256)          DEFAULT NULL,
    updated_last_at TIMESTAMP             DEFAULT NULL,

    deleted_by      VARCHAR(256)          DEFAULT NULL,
    deleted_at      TIMESTAMP             DEFAULT NULL,

    -- Files
    name            VARCHAR(256)          DEFAULT NULL,
    mimetype        VARCHAR(50)           DEFAULT NULL,
    path            VARCHAR(512)          DEFAULT NULL,
    size            NUMERIC(12, 3)        DEFAULT NULL, -- unit [bytes]

    -- Images
    width           NUMERIC(9, 3)         DEFAULT NULL, -- unit [px]
    height          NUMERIC(9, 3)         DEFAULT NULL,

    -- Products
    product_id      uuid                  DEFAULT NULL

);
