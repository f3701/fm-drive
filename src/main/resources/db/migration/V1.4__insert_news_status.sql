INSERT INTO drive.news_status (name, description)
VALUES ('ACTIVE', 'Active News (current)'),
       ('ARCHIVED', 'Archived News (should not be displayed to the general user)')
;