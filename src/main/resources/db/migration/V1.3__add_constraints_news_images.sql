ALTER TABLE drive.news_images
    ADD CONSTRAINT PK_news_images PRIMARY KEY (id),
    ADD CONSTRAINT FK_news_images_news_status FOREIGN KEY (news_status_id) REFERENCES drive.news_status(id)
 ;
