package com.dlabs.fm.drive.modules.files;

import com.dlabs.fm.drive.modules.files.exceptions.InvalidFormatFileException;
import com.dlabs.fm.drive.modules.files.services.ImageService;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ImageServiceTest {

     ImageService imageService = new ImageService();

    @Test
    void testFormatFileIsJPG() {

        MockMultipartFile file = new MockMultipartFile(
                "data",
                "filename.jpg",
                "image/jpg",
                "some jpg".getBytes()
        );

        imageService.validateFormat(file);
    }

    @Test
    void testFormatFileIsPNG() {

        MockMultipartFile file = new MockMultipartFile(
                "data",
                "filename.pdf",
                "application/pdf",
                "some pdf".getBytes()
        );

        assertThrows(
                InvalidFormatFileException.class,
                () -> imageService.validateFormat(file)
        );
    }

}