package com.dlabs.fm.drive.modules.news;

import com.dlabs.fm.drive.core.dtos.ResponseDto;
import com.dlabs.fm.drive.modules.news.controllers.NewsController;
import com.dlabs.fm.drive.modules.news.dtos.NewsImageInDto;
import com.dlabs.fm.drive.modules.news.dtos.NewsQueryParamsDto;
import com.dlabs.fm.drive.modules.news.services.NewsService;
import com.dlabs.fm.drive.modules.shared.URLs;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(NewsController.class)
class NewsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NewsService newsService;

    @Test
    void testEndpointForCreateNewsImageFile() throws Exception {

        final String ENDPOINT = URLs.BASE_URL + NewsController.UPLOAD_IMAGE_ENDPOINT;

        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "file",
                "originalFileName",
                MediaType.MULTIPART_FORM_DATA_VALUE,
                "file".getBytes()
        );
        assertNotNull(mockMultipartFile);

        when(newsService.create(mockMultipartFile))
                .thenReturn(ResponseDto.builder().build());


        mockMvc.perform(MockMvcRequestBuilders
                        .multipart(ENDPOINT)
                        .file(mockMultipartFile))
                .andExpect(status().isOk());
    }

    @Test
    void testEndpointForUpdateNewsImageFile() throws Exception {

        String UUID_STR = "123e4567-e89b-12d3-a456-556642440000";

        String ENDPOINT_RAW = URLs.BASE_URL + NewsController.UPDATE_IMAGE_DETAILS_ENDPOINT;
        String ENDPOINT_FINAL = ENDPOINT_RAW.replaceAll("\\{id}", UUID_STR);

        when(newsService.update(new NewsImageInDto(), UUID.randomUUID()))
                .thenReturn(ResponseDto.builder().build());


        mockMvc.perform(MockMvcRequestBuilders
                        .put(ENDPOINT_FINAL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"newsStatusId\":\"123e4567-e89b-12d3-a456-556642440000\"}"))
                .andExpect(status().isOk());
    }

    @Test
    void testEndpointForDeleteNewsImageFile() throws Exception {

        String UUID_STR = "123e4567-e89b-12d3-a456-556642440000";

        String ENDPOINT_RAW = URLs.BASE_URL + NewsController.DELETE_IMAGE_ENDPOINT;
        String ENDPOINT_FINAL = ENDPOINT_RAW.replaceAll("\\{id}", UUID_STR);

        when(newsService.delete( UUID.randomUUID()))
                .thenReturn(ResponseDto.builder().build());

        mockMvc.perform(MockMvcRequestBuilders
                        .delete(ENDPOINT_FINAL))
                .andExpect(status().isOk());
    }

    @Test
    void testEndpointForSearchNewsImageFiles() throws Exception {

        final String ENDPOINT_RAW = URLs.BASE_URL + NewsController.GET_IMAGES_DETAILS_ENDPOINT;
        final String ENDPOINT_FINAL = ENDPOINT_RAW + "?archived=true";

        when(newsService.search(new NewsQueryParamsDto()))
                .thenReturn(ResponseDto.builder().build());


        mockMvc.perform(MockMvcRequestBuilders
                        .get(ENDPOINT_FINAL))
                .andExpect(status().isOk());
    }

}
